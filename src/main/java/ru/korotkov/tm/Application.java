package ru.korotkov.tm;

import java.util.Arrays;
import java.util.Scanner;

import ru.korotkov.tm.constant.TerminalConst;
import ru.korotkov.tm.controller.*;
import ru.korotkov.tm.entity.Project;
import ru.korotkov.tm.entity.Task;
import ru.korotkov.tm.entity.User;
import ru.korotkov.tm.enumerated.Role;
import ru.korotkov.tm.repository.ProjectRepository;
import ru.korotkov.tm.repository.TaskRepository;
import ru.korotkov.tm.repository.UserRepository;
import ru.korotkov.tm.service.ProjectService;
import ru.korotkov.tm.service.ProjectTaskService;
import ru.korotkov.tm.service.TaskService;
import ru.korotkov.tm.service.UserService;

public class Application {

    private static final SystemController systemController = new SystemController();

    private final ProjectRepository projectRepository = new ProjectRepository();

    private final ProjectService projectService = new ProjectService(projectRepository);

    private final ProjectController projectController = new ProjectController(projectService);

    private final TaskRepository taskRepository = new TaskRepository();

    private final TaskService taskService = new TaskService(taskRepository);

    private final TaskController taskController = new TaskController(taskService);

    private final ProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final ProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService);

    private final UserRepository userRepository = new UserRepository();

    private final UserService userService = new UserService(userRepository);

    private final UserController userController = new UserController(userService);

    private Long userId;

    {
        User test = userService.create("TEST", "TEST");
        User admin = userService.create("ADMIN", "ADMIN", Role.ADMIN);
        Project project = projectService.create("1", "2", test.getId());
        Task task = taskService.create("3", "4", test.getId());
        task.setProjectId(project.getId());
    }

    public static void main(final String[] args) {
        final Application app = new Application();
        systemController.displayWelcome();
        app.run();
    }

    public void run() {
        Scanner scanner = new Scanner(System.in);
        String command;
        while (scanner.hasNextLine()) {
            command = scanner.nextLine();
            if (!process(command)) {
                break;
            }
        }
    }

    public boolean process(final String line) {
        if (line == null || line.isEmpty()) {
            return true;
        }

        if (TerminalConst.CMD_EXIT.equals(line)) {
            userId = null;
            userController.displayCloseSession();
            return false;
        }

        processCommand(line);
        return true;
    }

    private void processCommand(final String line) {
        final String parts[] = line.split(TerminalConst.SPLIT);
        final String command = parts[0];
        final String[] arguments = Arrays.copyOfRange(parts, 1, parts.length);
        switch (command) {
            case TerminalConst.CMD_VERSION:
                systemController.displayVersion();
                break;
            case TerminalConst.CMD_ABOUT:
                systemController.displayAbout();
                break;
            case TerminalConst.CMD_HELP:
                systemController.displayHelp();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject(arguments, userId);
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProject(userId);
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.listProject(userId);
                break;
            case TerminalConst.PROJECT_VIEW:
                projectController.viewProject(arguments, userId);
                break;
            case TerminalConst.PROJECT_REMOVE:
                projectController.displayProject(projectTaskController.removeProject(arguments, userId));
                break;
            case TerminalConst.PROJECT_UPDATE:
                projectController.updateProject(arguments, userId);
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask(arguments, userId);
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTask(userId);
                break;
            case TerminalConst.TASK_LIST:
                taskController.listTask(userId);
                break;
            case TerminalConst.TASK_VIEW:
                taskController.viewTask(arguments, userId);
                break;
            case TerminalConst.TASK_REMOVE:
                taskController.removeTask(arguments, userId);
                break;
            case TerminalConst.TASK_UPDATE:
                taskController.updateTask(arguments, userId);
                break;
            case TerminalConst.TASK_VIEW_BY_PROJECT:
                taskController.findTaskByProjectId(arguments, userId);
                break;
            case TerminalConst.TASK_ADD_TO_PROJECT:
                taskController.displayTask(projectTaskController.addTaskToProject(arguments, userId));
                break;
            case TerminalConst.TASK_REMOVE_FROM_PROJECT:
                taskController.removeTaskFromProject(arguments, userId);
                break;
            case TerminalConst.USER_CREATE:
                userController.createUser(arguments);
                break;
            case TerminalConst.USER_CLEAR:
                userController.clearUser();
                break;
            case TerminalConst.USER_LIST:
                userController.listUser();
                break;
            case TerminalConst.USER_VIEW:
                userController.viewUser(arguments);
                break;
            case TerminalConst.USER_REMOVE:
                userController.removeUser(arguments);
                break;
            case TerminalConst.USER_UPDATE:
                userController.updateUser(arguments);
                break;
            case TerminalConst.USER_EXIT:
                this.userId = null;
                userController.displayCloseSession();
                break;
            case TerminalConst.REGISTER:
                userController.register(arguments);
                break;
            case TerminalConst.AUTHENTICATION:
                User user = userController.authentication(arguments);
                if (user != null) {
                    this.userId = user.getId();
                }
                break;
            case TerminalConst.PROFILE_SHOW:
                userController.displayProfile(this.userId);
                break;
            case TerminalConst.PROFILE_UPDATE:
                userController.updateProfile(arguments, this.userId);
                break;
            case TerminalConst.PROFILE_CHANGE_PASSWORD:
                userController.changePassword(arguments, userId);
                break;
            default:
                systemController.displayStub(line);
                break;
        }
    }

}
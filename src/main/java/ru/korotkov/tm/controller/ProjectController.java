package ru.korotkov.tm.controller;

import ru.korotkov.tm.constant.TerminalConst;
import ru.korotkov.tm.entity.Project;
import ru.korotkov.tm.service.ProjectService;

public class ProjectController extends AbstractController {

    private final ProjectService projectService;

    public ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

    public void createProject(final String[] arguments, final Long userId) {
        final String name = arguments.length > 0 ? arguments[0] : null;
        final String description = arguments.length > 1 ? arguments[1] : null;
        if (description == null) {
            projectService.create(name, userId);
        } else {
            projectService.create(name, description, userId);
        }
        System.out.println(bundle.getString("projectCreate"));
    }

    public void clearProject(final Long userId) {
        projectService.clear(userId);
        System.out.println(bundle.getString("projectClear"));
    }

    public void viewProject(final String[] arguments, final Long userId) {
        final String option = arguments.length > 0 ? arguments[0] : null;
        final String param = arguments.length > 1 ? arguments[1] : null;
        if (option == null || param == null) {
            System.out.println(bundle.getString("commandSyntaxError"));
            return;
        }
        Project project = null;
        switch (option) {
            case TerminalConst.OPTION_INDEX:
                project = projectService.findByIndex(Integer.parseInt(param) - 1, userId);
                break;
            case TerminalConst.OPTION_NAME:
                project = projectService.findByName(param, userId);
                break;
            case TerminalConst.OPTION_ID:
                project = projectService.findById(Long.parseLong(param), userId);
                break;
        }
        displayProject(project);
    }

    public void displayProject(Project project) {
        if (project == null) {
            System.out.println(bundle.getString("notFound"));
            return;
        }

        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
    }

    public void updateProject(final String[] arguments, final Long userId) {
        final String option = arguments.length > 0 ? arguments[0] : null;
        final String param = arguments.length > 1 ? arguments[1] : null;
        final String name = arguments.length > 2 ? arguments[2] : null;
        final String description = arguments.length > 3 ? arguments[3] : null;
        if (option == null || param == null || name == null) {
            System.out.println(bundle.getString("commandSyntaxError"));
            return;
        }
        Project project = null;
        switch (option) {
            case TerminalConst.OPTION_INDEX:
                if (description == null) {
                    project = projectService.updateByIndex(Integer.parseInt(param) - 1, name, userId);
                } else {
                    project = projectService.updateByIndex(Integer.parseInt(param) - 1, name, description, userId);
                }
                break;
            case TerminalConst.OPTION_ID:
                if (description == null) {
                    project = projectService.updateById(Long.parseLong(param), name, userId);
                } else {
                    project = projectService.updateById(Long.parseLong(param), name, description, userId);
                }
                break;
        }
        displayProject(project);
    }

    public void listProject(final Long userId) {
        int index = 1;
        for (final Project project : projectService.findAll(userId)) {
            System.out.println("INDEX: " + index++ + " ID: " + project.getId() + " " + project.getName() + ": " + project.getDescription());
        }
    }

}

package ru.korotkov.tm.controller;

import ru.korotkov.tm.constant.TerminalConst;
import ru.korotkov.tm.entity.User;
import ru.korotkov.tm.enumerated.Role;
import ru.korotkov.tm.service.UserService;
import ru.korotkov.tm.util.HashUtil;

public class UserController extends AbstractController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    public void createUser(final String[] arguments) {
        final String login = arguments.length > 0 ? arguments[0] : "";
        final String password = arguments.length > 1 ? arguments[1] : "";
        userService.create(login, password);
        System.out.println(bundle.getString("userCreate"));
    }

    public void clearUser() {
        userService.clear();
        System.out.println(bundle.getString("userClear"));
    }

    public void viewUser(final String[] arguments) {
        final String option = arguments.length > 0 ? arguments[0] : null;
        final String param = arguments.length > 1 ? arguments[1] : null;
        if (option == null || param == null) {
            System.out.println(bundle.getString("commandSyntaxError"));
            return;
        }
        User user = null;
        switch (option) {
            case TerminalConst.OPTION_INDEX:
                user = userService.findByIndex(Integer.parseInt(param) - 1);
                break;
            case TerminalConst.OPTION_LOGIN:
                user = userService.findByLogin(param);
                break;
            case TerminalConst.OPTION_ID:
                user = userService.findById(Long.parseLong(param));
                break;
        }
        displayUser(user);
    }

    public void removeUser(final String[] arguments) {
        final String option = arguments.length > 0 ? arguments[0] : null;
        final String param = arguments.length > 1 ? arguments[1] : null;
        if (option == null || param == null) {
            System.out.println(bundle.getString("commandSyntaxError"));
            return;
        }
        User user = null;
        switch (option) {
            case TerminalConst.OPTION_INDEX:
                user = userService.removeByIndex(Integer.parseInt(param) - 1);
                break;
            case TerminalConst.OPTION_LOGIN:
                user = userService.removeByLogin(param);
                break;
            case TerminalConst.OPTION_ID:
                user = userService.removeById(Long.parseLong(param));
                break;
        }
        displayUser(user);
    }

    public void updateUser(final String[] arguments) {
        final String option = arguments.length > 0 ? arguments[0] : null;
        final String param = arguments.length > 1 ? arguments[1] : null;
        final String password = arguments.length > 2 ? arguments[2] : null;
        final String paramRole = arguments.length > 3 ? arguments[3] : null;
        Role role = null;
        for (final Role element : Role.values()) {
            if (element.toString().equals(paramRole)) {
                role = element;
                break;
            }
        }
        final String firstName = arguments.length > 4 ? arguments[4] : null;
        final String middleName = arguments.length > 5 ? arguments[5] : null;
        final String lastName = arguments.length > 6 ? arguments[6] : null;
        if (option == null || param == null) {
            System.out.println(bundle.getString("commandSyntaxError"));
            return;
        }
        User user = null;
        switch (option) {
            case TerminalConst.OPTION_INDEX:
                user = userService.updateByIndex(Integer.parseInt(param) - 1, password, role, firstName, middleName, lastName);
                break;
            case TerminalConst.OPTION_LOGIN:
                user = userService.updateByLogin(param, password, role, firstName, middleName, lastName);
                break;
            case TerminalConst.OPTION_ID:
                user = userService.updateById(Long.parseLong(param), password, role, firstName, middleName, lastName);
                break;
        }
        displayUser(user);
    }

    public User register(final String[] arguments)  {
        final String login = arguments.length > 0 ? arguments[0] : null;
        final String password = arguments.length > 1 ? arguments[1] : null;
        User user = userService.create(login, password);
        displayUser(user);
        return user;
    }

    public User changePassword(final String[] arguments, final Long userId)  {
        final String password = arguments.length > 0 ? arguments[0] : null;
        User user = userService.changePassword(password, userId);
        if (user != null) {
            System.out.println(bundle.getString("userChangePassword"));
        }
        return user;
    }

    public User authentication(final String[] arguments)  {
        final String login = arguments.length > 0 ? arguments[0] : null;
        final String password = arguments.length > 1 ? arguments[1] : null;
        User user = userService.authentication(login, password);
        if (user != null) {
            System.out.println(String.format(bundle.getString("userAuthenticated"), user.getLogin()));
        }
        return user;
    }


    public void displayUser(User user) {
        if (user == null) {
            System.out.println(bundle.getString("notFound"));
            return;
        }

        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("PASSWORD (HASH MD5): " + user.getPassword());
        System.out.println("FIRSTNAME: " + user.getFirstName());
        System.out.println("MIDDLENAME: " + user.getMiddleName());
        System.out.println("LASTNAME: " + user.getLastName());
        System.out.println("ROLE: " + user.getRole());
    }

    public void displayProfile(final Long userId) {
        User user = userService.findById(userId);
        if (user == null || !user.getId().equals(userId)) {
            return;
        }
        displayUser(user);
    }

    public void updateProfile(final String[] arguments, final Long userId) {
        final String firstName = arguments.length > 0 ? arguments[0] : null;
        final String middleName = arguments.length > 1 ? arguments[1] : null;
        final String lastName = arguments.length > 2 ? arguments[2] : null;
        User user = userService.updateProfile(userId, firstName, middleName, lastName);
        displayUser(user);
    }

    public void listUser() {
        int index = 1;
        for (final User user : userService.findAll()) {
            System.out.println("INDEX: " + index++ + " ID: " + user.getId() + " LOGIN: "
                    + user.getLogin() + " " + user.getFirstName() + " " + user.getLastName());
        }
    }

    public void displayCloseSession() {
        System.out.println(bundle.getString("userExit"));
    }

}

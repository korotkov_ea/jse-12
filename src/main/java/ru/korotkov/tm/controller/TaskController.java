package ru.korotkov.tm.controller;

import ru.korotkov.tm.constant.TerminalConst;
import ru.korotkov.tm.entity.Task;
import ru.korotkov.tm.service.TaskService;

public class TaskController extends AbstractController {

    private final TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    public void createTask(final String[] arguments, final Long userId) {
        final String name = arguments.length > 0 ? arguments[0] : "";
        final String description = arguments.length > 1 ? arguments[1] : "";
        if (description == null) {
            taskService.create(name, userId);
        } else {
            taskService.create(name, description, userId);
        }
        System.out.println(bundle.getString("taskCreate"));
    }

    public void clearTask(final Long userId) {
        taskService.clear(userId);
        System.out.println(bundle.getString("taskClear"));
    }

    public void viewTask(final String[] arguments, final Long userId) {
        final String option = arguments.length > 0 ? arguments[0] : null;
        final String param = arguments.length > 1 ? arguments[1] : null;
        if (option == null || param == null) {
            System.out.println(bundle.getString("commandSyntaxError"));
            return;
        }
        Task task = null;
        switch (option) {
            case TerminalConst.OPTION_INDEX:
                task = taskService.findByIndex(Integer.parseInt(param) - 1, userId);
                break;
            case TerminalConst.OPTION_NAME:
                task = taskService.findByName(param, userId);
                break;
            case TerminalConst.OPTION_ID:
                task = taskService.findById(Long.parseLong(param), userId);
                break;
        }
        displayTask(task);
    }

    public void displayTask(Task task) {
        if (task == null) {
            System.out.println(bundle.getString("notFound"));
            return;
        }

        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("PROJECTID: " + task.getProjectId());
    }

    public void removeTask(final String[] arguments, final Long userId) {
        final String option = arguments.length > 0 ? arguments[0] : null;
        final String param = arguments.length > 1 ? arguments[1] : null;
        if (option == null || param == null) {
            System.out.println(bundle.getString("commandSyntaxError"));
            return;
        }
        Task task = null;
        switch (option) {
            case TerminalConst.OPTION_INDEX:
                task = taskService.removeByIndex(Integer.parseInt(param) - 1, userId);
                break;
            case TerminalConst.OPTION_NAME:
                task = taskService.removeByName(param, userId);
                break;
            case TerminalConst.OPTION_ID:
                task = taskService.removeById(Long.parseLong(param), userId);
                break;
        }
        displayTask(task);
    }

    public void updateTask(final String[] arguments, final Long userId) {
        final String option = arguments.length > 0 ? arguments[0] : null;
        final String param = arguments.length > 1 ? arguments[1] : null;
        final String name = arguments.length > 2 ? arguments[2] : null;
        final String description = arguments.length > 3 ? arguments[3] : null;
        if (option == null || param == null || name == null) {
            System.out.println(bundle.getString("commandSyntaxError"));
            return;
        }
        Task task = null;
        switch (option) {
            case TerminalConst.OPTION_INDEX:
                if (description == null) {
                    task = taskService.updateByIndex(Integer.parseInt(param) - 1, name, userId);
                } else {
                    task = taskService.updateByIndex(Integer.parseInt(param) - 1, name, description, userId);
                }
                break;
            case TerminalConst.OPTION_ID:
                if (description == null) {
                    task = taskService.updateById(Long.parseLong(param), name, userId);
                } else {
                    task = taskService.updateById(Long.parseLong(param), name, description, userId);
                }
                break;
        }
        displayTask(task);
    }

    public void listTask(final Long userId) {
        int index = 1;
        for (final Task task : taskService.findAll(userId)) {
            System.out.println("INDEX: " + index++ + " ID: "
                    + task.getId() + " PROJECTID: " + task.getProjectId()
                    + " " + task.getName() + ": " + task.getDescription());
        }
    }

    public void findTaskByProjectId(final String[] arguments, final Long userId) {
        final Long projectId = arguments.length > 0 ? Long.parseLong(arguments[0]) : null;
        for (final Task task: taskService.findTasksByProjectId(projectId, userId)) {
            displayTask(task);
        }
    }

    public void removeTaskFromProject(final String[] arguments, final Long userId) {
        final Long taskId = arguments.length > 0 ? Long.parseLong(arguments[0]) : null;
        Task task = taskService.removeTaskFromProject(taskId, userId);
        displayTask(task);
    }

}
